package com.silverline.Kite.BaseTests;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import com.silverline.Kite.PO.*;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import com.silverline.Kite.Methods.MainMethods;
import com.silverline.Kite.Helpers.BrowsersHelper;

public class BaseTests {

    public static WebDriver driver;
    public static LoginPage lp;
    public static MainMethods mm;
    public static HomePage hp;
    public static PatientForm pf;

    @BeforeClass
    public static void setup() {
        //System.setProperty("webdriver.chrome.driver", "D:\\2018_!!!\\comsilverlineCafs\\chromedriver.exe");
        //driver = new ChromeDriver();
        //driver = BrowsersHelper.getChromeLocalWebDriver();
        driver = BrowsersHelper.getChromeRemoteWebDriver();
        lp = new LoginPage(driver);
        mm = new MainMethods(driver);
        hp = new HomePage(driver);
        pf = new PatientForm(driver);
        driver.manage().window().maximize();
        driver.get("https://full-kitekonnect.cs96.force.com");
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.MINUTES);
        lp.LoginToApplication();
        try {
            hp.NavigateToPortalHome();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @AfterClass
    public static void end() {
        driver.quit();
    }
}
