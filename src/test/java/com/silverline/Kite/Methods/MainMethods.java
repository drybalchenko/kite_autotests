package com.silverline.Kite.Methods;

import com.silverline.Kite.BaseTests.BaseTests;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

public class MainMethods extends BaseTests{

    public MainMethods(WebDriver driver) {
        PageFactory.initElements(driver, this);
        BaseTests.driver = driver;
    }

    WebDriverWait wait = new WebDriverWait(driver, 30);

    public void FillField(WebElement field, String value){
        wait.until(ExpectedConditions.elementToBeClickable(field));;
        field.clear();
        field.sendKeys(value);
    }

    public void ClickOn (WebElement element){
        wait.until(ExpectedConditions.elementToBeClickable(element));
        //element.click();
        element.sendKeys(Keys.ENTER);
    }

    //---

    //---METHODS
    public void CheckLightning(){
        //driver.manage().timeouts().implicitlyWait(1, TimeUnit.MINUTES);
        WebElement lButton = driver.findElement(By.xpath("//a[@class='switch-to-lightning']"));
        if (lButton.isDisplayed()){
            lButton.click();
        }
    }

    public void WaitForHomePage(){
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.MINUTES);
        driver.findElement(By.className("slds-global-header__logo"));
    }

    public Boolean IsElementPresent(WebElement field){
        return field.isDisplayed() && field.isEnabled();
    }

    public void CheckFieldName(String name, WebElement namePlace){
        assertTrue("Incorrect name of field" + name,name == namePlace.getText());
    }

    public void SelectValueFromPicklist(WebElement field, WebElement value){
        field.isDisplayed();
        field.isEnabled();
        field.click();
        value.click();
    }

    public void SelectFromPicklist(WebElement field, int recordNumber) throws InterruptedException {
        field.sendKeys(Keys.ENTER);
        Thread.sleep(750);
        while (recordNumber>0) {
            Thread.sleep(100);
            field.sendKeys(Keys.ARROW_DOWN);
            recordNumber--;
        }
        field.sendKeys(Keys.ENTER);
    }

    public void SelectFromSearchField(WebElement field, WebElement firstValue, String value) throws InterruptedException {
        field.click();
        //field.clear();
        FillField(field, value);
        Thread.sleep(2000);
        firstValue.click();
    }

    public void CheckText(WebElement place, String text){
        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //place.isDisplayed();
        String existingValue = place.getText();
        Assert.assertTrue("Incorrect value. Expected:"+text +", Actual:"+existingValue, existingValue.equals(text));
    }

    public void NavigateToNextPage(WebElement button, WebElement elementOnPage) {
        button.sendKeys(Keys.ENTER);
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.MINUTES);
        elementOnPage.isDisplayed();
    }

    public void CheckErrorMessage(WebElement place, String message){
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        place.isDisplayed();
        String errorText = place.getText();
        Assert.assertTrue("Incorrect Error message. Expected:"+message +", Actual:"+errorText, errorText.equals(message));
    }

    public void WaitForElement(WebElement element){
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }
}
