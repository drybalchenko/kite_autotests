package com.silverline.Kite.Helpers;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;

public class BrowsersHelper {
    private static String remoteURL = "http://marina67:rDnzuyq1edYevNDxbgNL@hub.browserstack.com/wd/hub";

    public static WebDriver getIE11LocalWebDriver () {
        System.setProperty( "webdriver.ie.driver", "D://tools//IEDriverServer.exe");
        DesiredCapabilities capability = new DesiredCapabilities();
        capability.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true);
//		capability.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false);
        WebDriver driver = new InternetExplorerDriver(capability);
        driver.manage().window().maximize();
        return driver;
    }

    public static WebDriver getChromeLocalWebDriver () {
        //System.setProperty("webdriver.chrome.driver", "D://chromedriver2_33.exe");
        System.setProperty("webdriver.chrome.driver", "C://tools//chromedriver_win32//chromedriver2_33.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        return driver;
    }

    public static WebDriver getIE11RemoteWebDriver () {
        DesiredCapabilities capability = new DesiredCapabilities();
        capability.setCapability("browser", "IE");
        capability.setCapability("browser_version", "11.0");
//		capability.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true);
//		capability.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false);
        capability.setCapability("os", "Windows");
        capability.setCapability("os_version", "7");
        capability.setCapability("build", "CalenarAnything");
        capability.setCapability("acceptSslCerts", "true");
        capability.setCapability("browserstack.debug", "true");
        capability.setCapability("resolution", "1280x1024");
        capability.setCapability("browserstack.video", "true");
//		capability.setCapability("browserstack.ie.driver", "2.52");
        RemoteWebDriver driver = null;
        try {
            driver = new RemoteWebDriver(
                    new URL(remoteURL),	capability);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        driver.setFileDetector(new LocalFileDetector());
        driver.manage().window().maximize();
        return driver;
    }

    public static WebDriver getIE9RemoteWebDriver () {
        DesiredCapabilities capability = new DesiredCapabilities();
        capability.setCapability("browser", "IE");
        capability.setCapability("browser_version", "9.0");
        capability.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true);
        capability.setCapability("os", "Windows");
        capability.setCapability("os_version", "7");
        capability.setCapability("build", "CalenarAnything");
        capability.setCapability("acceptSslCerts", "true");
        capability.setCapability("browserstack.debug", "true");
        capability.setCapability("resolution", "1280x1024");
//		capability.setCapability("resolution", "1366x768");
        capability.setCapability("browserstack.video", "false");
        capability.setCapability("browserstack.ie.driver", "2.46");
        RemoteWebDriver driver = null;
        try {
            driver = new RemoteWebDriver(
                    new URL(remoteURL),	capability);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        driver.setFileDetector(new LocalFileDetector());
        driver.manage().window().maximize();
        return driver;
    }

    public static WebDriver getChromeRemoteWebDriver () {

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-extensions");

        DesiredCapabilities capability = new DesiredCapabilities();
        capability.setCapability("browser", "chrome");
        capability.setCapability("os", "Windows");
        capability.setCapability("os_version", "7");
        capability.setCapability("build", "CalenarAnything");
        capability.setCapability("acceptSslCerts", "true");
        capability.setCapability("browserstack.debug", "true");
        capability.setCapability("resolution", "1280x1024");
        capability.setCapability("browserstack.video", "false");
        capability.setCapability(ChromeOptions.CAPABILITY, options);
        RemoteWebDriver driver = null;
        try {
            driver = new RemoteWebDriver(
                    new URL(remoteURL),	capability);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        driver.setFileDetector(new LocalFileDetector());
        driver.manage().window().maximize();
        return driver;
    }

    public static WebDriver getSafariRemoteWebDriver () {
        DesiredCapabilities capability = new DesiredCapabilities();
        capability.setCapability("browser", "Safari");
        capability.setCapability("browser_version", "8.0");
        capability.setCapability("os", "OS X");
        capability.setCapability("os_version", "Yosemite");
        capability.setCapability("build", "CalenarAnything");
        capability.setCapability("acceptSslCerts", "true");
        capability.setCapability("browserstack.debug", "true");
        capability.setCapability("resolution", "1280x1024");
        capability.setCapability("browserstack.video", "false");
        RemoteWebDriver driver = null;
        try {
            driver = new RemoteWebDriver(
                    new URL(remoteURL),	capability);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        driver.setFileDetector(new LocalFileDetector());
        driver.manage().window().maximize();
        return driver;
    }
}
