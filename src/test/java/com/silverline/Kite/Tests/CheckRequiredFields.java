package com.silverline.Kite.Tests;

import com.silverline.Kite.BaseTests.BaseTests;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

public class CheckRequiredFields extends BaseTests{

    @Test
    public void KITE01_EmptyPatientForm() throws InterruptedException {
        System.out.println("[INFO]: Test 'KITE01 Save Empty Patient Form' - START >>>");
        driver.get("https://full-kitekonnect.cs96.force.com/s/portalhome");
        //hp.NavigateToPortalHome();
        hp.NavigateToPatientForm();
        mm.ClickOn(pf.SubmitButton);
        mm.CheckErrorMessage(pf.ErrorForFirstNameField, pf.ErrorMessage);
        mm.CheckErrorMessage(pf.ErrorForLastNameField, pf.ErrorMessage);
        mm.CheckErrorMessage(pf.ErrorForDobYearField, "Invalid year");
        mm.CheckErrorMessage(pf.ErrorForMonthField, "Invalid month");
        mm.CheckErrorMessage(pf.ErrorForDayField, "Invalid day");
        mm.CheckErrorMessage(pf.ErrorForGender, pf.ErrorMessage);
        mm.CheckErrorMessage(pf.ErrorForThreatingPhisicianField, pf.ErrorMessage);
        mm.CheckErrorMessage(pf.ErrorForAddOwnHp, pf.ErrorMessage);
        System.out.println("[INFO]: Test 'KITE01 Save Empty Patient Form' - PASSED <<<");
    }

    @Test
    public void KITE02_NewPatientWithoutFirstName() throws InterruptedException {
        System.out.println("[INFO]: Test 'KITE02 Save Patient Without First Name' - START >>>");
        driver.get("https://full-kitekonnect.cs96.force.com/s/portalhome");
        hp.NavigateToPatientForm();
        mm.FillField(pf.LastNameField, pf.LastName);
        mm.FillField(pf.DobYearField, pf.DobYear);
        mm.SelectFromPicklist(pf.MonthField, 5);
        mm.SelectFromPicklist(pf.DayField, 11);
        pf.MaleGenderButton.click();
        mm.SelectFromSearchField(pf.ThreatingPhisicianField, pf.ThreatingFirstRecord, pf.ThreatingPhysician);
        pf.AddOwnHpNoButton.click();
        mm.ClickOn(pf.SubmitButton);
        mm.CheckErrorMessage(pf.ErrorForFirstNameField, pf.ErrorMessage);
        System.out.println("[INFO]: Test 'KITE02 Save Patient Without First Name' - PASSED <<<");
    }

    @Test
    public void KITE03_NewPatientWithoutLastName() throws InterruptedException {
        System.out.println("[INFO]: Test 'KITE03 Save Patient Without Last Name' - START >>>");
        driver.get("https://full-kitekonnect.cs96.force.com/s/portalhome");
        hp.NavigateToPatientForm();
        mm.FillField(pf.FirstNameField, pf.FirstName);
        mm.FillField(pf.DobYearField, pf.DobYear);
        mm.SelectFromPicklist(pf.MonthField, 5);
        mm.SelectFromPicklist(pf.DayField, 11);
        pf.MaleGenderButton.click();
        mm.SelectFromSearchField(pf.ThreatingPhisicianField, pf.ThreatingFirstRecord, pf.ThreatingPhysician);
        pf.AddOwnHpNoButton.click();
        mm.ClickOn(pf.SubmitButton);
        mm.CheckErrorMessage(pf.ErrorForLastNameField, pf.ErrorMessage);
        System.out.println("[INFO]: Test 'KITE03 Save Patient Without Last Name' - PASSED <<<");
    }

    @Test
    public void KITE04_NewPatientWithoutYear() throws InterruptedException {
        System.out.println("[INFO]: Test 'KITE04 Save Patient Without Date Of Birthday (Year/Day)' - START >>>");
        driver.get("https://full-kitekonnect.cs96.force.com/s/portalhome");
        hp.NavigateToPatientForm();
        mm.FillField(pf.FirstNameField, pf.FirstName);
        mm.FillField(pf.LastNameField, pf.LastName);
        mm.SelectFromPicklist(pf.MonthField, 5);
        pf.MaleGenderButton.click();
        mm.SelectFromSearchField(pf.ThreatingPhisicianField, pf.ThreatingFirstRecord, pf.ThreatingPhysician);
        pf.AddOwnHpNoButton.click();
        mm.ClickOn(pf.SubmitButton);
        mm.CheckErrorMessage(pf.ErrorForDobYearField, "Invalid year");
        mm.CheckErrorMessage(pf.ErrorForDayField, "Invalid day");
        System.out.println("[INFO]: Test 'KITE04 Save Patient Without Date Of Birthday (Year/Day)' - PASSED <<<");
    }

    @Test
    public void KITE05_NewPatientWithoutMonth() throws InterruptedException {
        System.out.println("[INFO]: Test 'KITE05 Save Patient Without Date Of Birthday (Month/Day)' - START >>>");
        driver.get("https://full-kitekonnect.cs96.force.com/s/portalhome");
        hp.NavigateToPatientForm();
        mm.FillField(pf.FirstNameField, pf.FirstName);
        mm.FillField(pf.LastNameField, pf.LastName);
        mm.FillField(pf.DobYearField, pf.DobYear);
        pf.MaleGenderButton.click();
        mm.SelectFromSearchField(pf.ThreatingPhisicianField, pf.ThreatingFirstRecord, pf.ThreatingPhysician);
        pf.AddOwnHpNoButton.click();
        mm.ClickOn(pf.SubmitButton);
        mm.CheckErrorMessage(pf.ErrorForMonthField, "Invalid month");
        mm.CheckErrorMessage(pf.ErrorForDayField, "Invalid day");
        System.out.println("[INFO]: Test 'KITE05 Save Patient Without Date Of Birthday (Month/Day)' - PASSED <<<");
    }

    @Test
    public void KITE06_NewPatientWithoutDay() throws InterruptedException {
        System.out.println("[INFO]: Test 'KITE06 Save Patient Without Date Of Birthday (Day)' - START >>>");
        driver.get("https://full-kitekonnect.cs96.force.com/s/portalhome");
        hp.NavigateToPatientForm();
        mm.FillField(pf.FirstNameField, pf.FirstName);
        mm.FillField(pf.LastNameField, pf.LastName);
        mm.FillField(pf.DobYearField, pf.DobYear);
        mm.SelectFromPicklist(pf.MonthField, 5);
        pf.MaleGenderButton.click();
        mm.SelectFromSearchField(pf.ThreatingPhisicianField, pf.ThreatingFirstRecord, pf.ThreatingPhysician);
        pf.AddOwnHpNoButton.click();
        mm.ClickOn(pf.SubmitButton);
        mm.CheckErrorMessage(pf.ErrorForDayField, "Invalid day");
        System.out.println("[INFO]: Test 'KITE06 Save Patient Without Date Of Birthday (Day)' - PASSED <<<");
    }

    @Test
    public void KITE07_NewPatientWithoutGender() throws InterruptedException {
        System.out.println("[INFO]: Test 'KITE07 Save Patient Without Gender' - START >>>");
        driver.get("https://full-kitekonnect.cs96.force.com/s/portalhome");
        hp.NavigateToPatientForm();
        mm.FillField(pf.FirstNameField, pf.FirstName);
        mm.FillField(pf.LastNameField, pf.LastName);
        mm.FillField(pf.DobYearField, pf.DobYear);
        mm.SelectFromPicklist(pf.MonthField, 5);
        mm.SelectFromPicklist(pf.DayField, 11);
        mm.SelectFromSearchField(pf.ThreatingPhisicianField, pf.ThreatingFirstRecord, pf.ThreatingPhysician);
        pf.AddOwnHpNoButton.click();
        mm.ClickOn(pf.SubmitButton);
        mm.CheckErrorMessage(pf.ErrorForGender, pf.ErrorMessage);
        System.out.println("[INFO]: Test 'KITE07 Save Patient Without Gender' - PASSED <<<");
    }

    @Test
    public void KITE08_NewPatientWithoutTreatingPhysician() throws InterruptedException {
        System.out.println("[INFO]: Test 'KITE08 Save Patient Without Treating Physician' - START >>>");
        driver.get("https://full-kitekonnect.cs96.force.com/s/portalhome");
        hp.NavigateToPatientForm();
        mm.FillField(pf.FirstNameField, pf.FirstName);
        mm.FillField(pf.LastNameField, pf.LastName);
        mm.FillField(pf.DobYearField, pf.DobYear);
        mm.SelectFromPicklist(pf.MonthField, 5);
        mm.SelectFromPicklist(pf.DayField, 11);
        pf.MaleGenderButton.click();
        pf.AddOwnHpNoButton.click();
        mm.ClickOn(pf.SubmitButton);
        mm.CheckErrorMessage(pf.ErrorForThreatingPhisicianField, pf.ErrorMessage);
        System.out.println("[INFO]: Test 'KITE08 Save Patient Without Treating Physician' - PASSED <<<");
    }

    @Test
    public void KITE09_NewPatientWithoutHospitalPatientId() throws InterruptedException {
        System.out.println("[INFO]: Test 'KITE09 Save Patient Without Hospital Patient ID' - START >>>");
        driver.get("https://full-kitekonnect.cs96.force.com/s/portalhome");
        hp.NavigateToPatientForm();
        mm.FillField(pf.FirstNameField, pf.FirstName);
        mm.FillField(pf.LastNameField, pf.LastName);
        mm.FillField(pf.DobYearField, pf.DobYear);
        mm.SelectFromPicklist(pf.MonthField, 5);
        mm.SelectFromPicklist(pf.DayField, 11);
        pf.MaleGenderButton.click();
        mm.SelectFromSearchField(pf.ThreatingPhisicianField, pf.ThreatingFirstRecord, pf.ThreatingPhysician);
        mm.ClickOn(pf.SubmitButton);
        mm.CheckErrorMessage(pf.ErrorForAddOwnHp, pf.ErrorMessage);
        System.out.println("[INFO]: Test 'KITE09 Save Patient Without Hospital Patient ID' - PASSED <<<");
    }
}
