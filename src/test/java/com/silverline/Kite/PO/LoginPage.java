package com.silverline.Kite.PO;

import com.silverline.Kite.BaseTests.BaseTests;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class LoginPage extends BaseTests{

    public LoginPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        BaseTests.driver = driver;
    }

    WebDriverWait wait = new WebDriverWait(driver, 30);

    public String UserLogin = "kite@silverlinecrm.com.full";
    public String UserPassword = "$ummerTwo108";

    @FindBy(xpath = "//a[@class='slds-button slds-button--brand ' and contains (text(), 'Sign In')]")
    public WebElement SingInButton;

    @FindBy(xpath = "//input[@data-interactive-lib-uid='2']")
    public WebElement LoginField;

    @FindBy(xpath = "//input[@type='password']")
    public WebElement PasswordField;

    @FindBy(xpath = "//button[@class='slds-button slds-button--brand' and contains (text(), 'Sign In')]")
    public WebElement LoginButton;

    //---Methods

    public void LoginToApplication (){
        mm.ClickOn(SingInButton);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        mm.FillField(LoginField, UserLogin);
        mm.FillField(PasswordField, UserPassword);
        mm.ClickOn(LoginButton);
        wait.until(ExpectedConditions.elementToBeClickable(hp.InstitutionSearchField));
        //hp.KiteLogo.isDisplayed();
    }
}
