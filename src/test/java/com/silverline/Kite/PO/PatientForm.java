package com.silverline.Kite.PO;

import com.silverline.Kite.BaseTests.BaseTests;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PatientForm extends BaseTests{

    public PatientForm(WebDriver driver) {
        PageFactory.initElements(driver, this);
        BaseTests.driver = driver;
    }

    public String ErrorMessage = "This field is required";

    //---Patient data
    public String FirstName = "Test";
    public String MiddleName = "super";
    public String LastName = "User";
    public String DobYear = "1990";
    public String ThreatingPhysician = "Test Doctor";

    @FindBy(xpath = "//*[contains(text(), 'Please Register Your Patient')]")
    public WebElement PatientForm;

    @FindBy(xpath = "//input[@data-interactive-lib-uid='2']")
    public WebElement FirstNameField;

    @FindBy(xpath = "//div[@class='slds-grid slds-wrap slds-grid--pull-padded cCmp_SL_PatientDemographics_Edit'][1]/div[@class='slds-small-size--4-of-12 slds-medium-size--5-of-12 slds-large-size--2-of-6  slds-p-around--medium cSL_GridColumn'][1]//span[@class='slds-form-element__help cSL_FormElementError']")
    public WebElement ErrorForFirstNameField;

    @FindBy(xpath = "//input[@data-interactive-lib-uid='3']")
    public WebElement MiddleNameField;

    @FindBy(xpath = "//input[@data-interactive-lib-uid='4']")
    public WebElement LastNameField;

    @FindBy(xpath = "//div[@class='slds-grid slds-wrap slds-grid--pull-padded cCmp_SL_PatientDemographics_Edit'][1]/div[@class='slds-small-size--4-of-12 slds-medium-size--5-of-12 slds-large-size--2-of-6  slds-p-around--medium cSL_GridColumn'][2]//span[@class='slds-form-element__help cSL_FormElementError']")
    public WebElement ErrorForLastNameField;

    @FindBy(xpath = "//input[@data-interactive-lib-uid='5']")
    public WebElement DobYearField;

    @FindBy(xpath = "//div[@class='slds-kite__content']/div[3]/div[1]/div/span")
    public WebElement ErrorForDobYearField;

    @FindBy(xpath = "//div[@class='slds-kite__content']/div[3]/div[2]/div/div/div/select")
    public WebElement MonthField;

    @FindBy(xpath = "//div[@class='slds-kite__content']/div[3]/div[2]/div/span")
    public WebElement ErrorForMonthField;

    @FindBy(xpath = "//div[@class='slds-kite__content']/div[3]/div[3]/div/div/div/select")
    public WebElement DayField;

    @FindBy(xpath = "//div[@class='slds-kite__content']/div[3]/div[3]/div/span")
    public WebElement ErrorForDayField;

    @FindBy(css = "[class='slds-small-size--3-of-12 slds-medium-size--3-of-12 slds-large-size--3-of-12  slds-p-around--medium cSL_GridColumn'] .slds-radio--button:nth-of-type(1) .slds-radio--button__label")
    public WebElement MaleGenderButton;

    @FindBy(css = ".cSL_Radio:nth-child(1) .slds-radio--button:nth-child(2)")
    public WebElement FemaleGenderButton;

    @FindBy(xpath = "//div[@class='slds-kite__content']/div[3]/div[4]/fieldset/span")
    public WebElement ErrorForGender;

    @FindBy(id = "lookatme")
    public WebElement ThreatingPhisicianField;

    @FindBy(xpath = "//li[@data-selindx='0']")
    public WebElement ThreatingFirstRecord;

    @FindBy(xpath = "//div[@class='slds-kite__content']/div[4]/div[1]/div/span")
    public WebElement ErrorForThreatingPhisicianField;

    @FindBy(xpath = "//div[@id='lookatme']/ul/li[@data-selindx='0']")
    public WebElement ThreatingPhisicianFirstRecord;

    @FindBy(xpath = "//div[@class='slds-kite__content']/div[5]/div//div[@class='slds-form-element__control']/div/span[1]/label[@class='slds-radio--button__label']")
    public WebElement AddOwnHpYesButton;

    @FindBy(xpath = "//div[@class='slds-kite__content']/div[5]/div//div[@class='slds-form-element__control']/div/span[2]/label[@class='slds-radio--button__label']")
    public WebElement AddOwnHpNoButton;

    @FindBy(xpath = "//div[@class='slds-kite__content']/div[5]/div[1]/fieldset/span")
    public WebElement ErrorForAddOwnHp;

    //---Buttons
    @FindBy(xpath = "//button[@class='slds-button slds-button--brand slds-p-left--small slsavenext kite-button__long' and contains(text(), 'Submit')]")
    public WebElement SubmitButton;

    @FindBy(xpath = "//button[@class='slds-button slds-button--neutral kite-button__long' and contains(text(), 'Cancel')]")
    public WebElement CancelButton;
    //---

    @FindBy(xpath = "")
    public WebElement IdTypeField;

    @FindBy(xpath = "//input[@data-interactive-lib-uid='7']")
    public WebElement PatientIDField;

    //---TOTAL PAGE-----------------------------------------------------------------------------------------------------

    @FindBy(xpath = "//div[@class='slds-grid slds-p-vertical--medium'][1]//span[@id='patientNameView']")
    public WebElement PatientNameView;

    @FindBy(xpath = "//div[@class='slds-grid slds-p-vertical--medium'][2]//span[@id='patientNameView']")
    public WebElement PatientBirthdayView;

    @FindBy(xpath = "//div[@class='slds-modal__content slds-p-around--medium']/div[3]/div[2]//span[@id='patientNameView']")
    public WebElement PatientGenderView;

    //---Buttons
    @FindBy(xpath = "//button[@class='slds-button slds-button--neutral' and contains(text(), 'Back')]")
    public WebElement BackButton;

    @FindBy(xpath = "//button[@class='slds-button slds-button--neutral' and contains(text(), 'Confirm and Continue to Cell Order')]")
    public WebElement ConfirmButton;

}
