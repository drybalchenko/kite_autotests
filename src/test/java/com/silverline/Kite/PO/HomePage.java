package com.silverline.Kite.PO;

import com.silverline.Kite.BaseTests.BaseTests;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class HomePage extends BaseTests{

    public HomePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        BaseTests.driver = driver;
    }

    WebDriverWait wait = new WebDriverWait(driver, 30);

    public String Institution = "Test Cancer Center";
    public String User = "Test HCP";

    @FindBy(xpath = "//img[@src='/resource/1497545203000/NewLogo']")
    public WebElement KiteLogo;

    @FindBy(xpath = "//input[@id='lookatme']")
    public WebElement InstitutionSearchField;

    @FindBy(xpath = "//li[@data-selindx='0']")
    public WebElement InstitutionFirstRecord;

    @FindBy(xpath = "//div[@class='button-wrap'][2]//div[@class='slds-form-element__control']//input")
    public WebElement UserSearchField;

    @FindBy(xpath = "//div[@class='button-wrap'][2]//div[@class='slds-lookup__menu']/ul/li[2]")
    public WebElement UserFirstRecord;

    @FindBy(xpath = "//div[@class='slds-spinner_container cCmp_SL_PatientTable']")
    public WebElement SpinnerIcon;

    @FindBy(xpath = "//button[@class='slds-button slds-button--brand']")
    public WebElement LogInToButton;

    //---Center

    @FindBy(xpath = "//*[contains(text(), 'Enroll New Patient')]")
    public WebElement EnrollNewPatientButton;

    //---Methods

    public void NavigateToPortalHome() throws InterruptedException{
        //driver.get("https://full-kitekonnect.cs96.force.com/s/portalhome");
        mm.SelectFromSearchField(InstitutionSearchField, InstitutionFirstRecord, Institution);
        mm.SelectFromSearchField(UserSearchField, UserFirstRecord, User);
        LogInToButton.click();
        //EnrollNewPatientButton.isDisplayed();
        wait.until(ExpectedConditions.elementToBeClickable(EnrollNewPatientButton));
    }

    public void NavigateToPatientForm() throws InterruptedException {
        wait.until(ExpectedConditions.elementToBeClickable(EnrollNewPatientButton));
        Thread.sleep(1500);
        hp.EnrollNewPatientButton.click();
        pf.PatientForm.isDisplayed();
    }
}
